{-# LANGUAGE OverloadedStrings #-}
module Dependencies where

import Data.Telegram (telegramWreqClient)
import Domain.Boundaries (TelegramClient (..))

mockedTelegramClient = TelegramClient
  { sendCode = \_ -> return "potato"
  , sendMessage = \p m -> return $ p <> ": " <> m
  , getChats = return ["One", "Two", "Three"]
  , getChat = \p -> return [(p, "a")]
  }

telegramClient = telegramWreqClient

