{-# LANGUAGE OverloadedStrings, ExtendedDefaultRules, FlexibleContexts, RankNTypes #-}
module Main where
import Prelude hiding (on, (<+>))

import Brick.Main -- (App (..), appDraw, appChooseCursor, appHandleEvent, appStartEvent, appAttrMap, defaultMain)
import Brick.Types -- (Widget, CursorLocation, Padding (..))
import Brick.AttrMap (AttrMap, attrMap)
import Brick.Widgets.Core -- (txt, (<+>), (<=>), hLimit, vLimit, hBox, vBox, padLeft, padRight)
import Brick.Widgets.Edit (renderEditor, editAttr, editFocusedAttr, editor)
import Brick.Focus (withFocusRing, focusRingCursor, focusRing)
import Brick.Util (on, clamp)
import Brick.Widgets.Border (hBorder)
import Brick.Widgets.Center (center, vCenter, hCenter)

import Graphics.Vty (defAttr, white, blue, black, yellow, brightBlack, brightWhite, rgbColor)
import Lens.Micro ((^.), (%~))

import Presentation.Event (event)
import Presentation.State
import Control.Monad.Reader (withReaderT)

vLimitPercent :: Int -> Widget n -> Widget n
vLimitPercent h' p =
    Widget (vSize p) Fixed $ do
      let h = clamp 0 100 h'
      ctx <- getContext
      let usableHeight = ctx^.availHeightL
          widgetHeight = round (toRational usableHeight * (toRational h / 100))
      withReaderT (availHeightL %~ min widgetHeight) $ render $ cropToContext p

draw :: State -> [Widget Name]
draw state = [ui]
  where
    constructView view = withFocusRing (state ^. mainFocus) (renderEditor (txt . unlines)) (state ^. view)

    receiverRow = padRight (Pad 10) . vBox $ [txt "Receiver: ", hLimit 30 (vLimit 5 (constructView test))]
    messageRow  = padLeft  (Pad 20) . vBox $ [txt "Message:  ", vLimit 5 (constructView message)]

    ui = vBox
      [ hCenter receiverRow
      , vLimitPercent 60 $ vBox [txt "This is some text", fill ' ', hBorder]
      , center messageRow
      ]

cursor :: State -> [CursorLocation Name] -> Maybe (CursorLocation Name)
cursor = focusRingCursor (^. mainFocus)

attributes :: AttrMap
attributes = attrMap defAttr
  [ (editAttr,        white `on` brightWhite)
  , (editFocusedAttr, white `on` rgbColor 43 43 43)
  ]

appSetup :: App State e Name
appSetup = App
  { appDraw = draw
  , appChooseCursor = cursor
  , appHandleEvent = event
  , appStartEvent = return
  , appAttrMap = const attributes
  }

initialState :: State
initialState =
  State (focusRing [Message, Test])
        (editor Message Nothing "")
        (editor Test Nothing "")

main :: IO ()
main = void $ defaultMain appSetup initialState

