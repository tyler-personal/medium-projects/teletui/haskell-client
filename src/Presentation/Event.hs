{-# LANGUAGE RankNTypes #-}
module Presentation.Event (event) where

import Brick.Main (halt, continue)
import Brick.Types (BrickEvent (VtyEvent), EventM, Next, handleEventLensed)
import Brick.Widgets.Edit (Editor, handleEditorEvent, getEditContents, applyEdit)
import Brick.Focus(focusGetCurrent, focusPrev, focusNext)

import Graphics.Vty (Event (EvKey), Key (..))
import Lens.Micro (Lens', (%~), (^.))
import Data.Text.Zipper (clearZipper)

import Domain.Boundaries (TelegramClient (..))
import Presentation.State
import Dependencies (telegramClient)


event :: State -> BrickEvent Name e -> EventM Name (Next State)
event state (VtyEvent event) = case event of
  EvKey (KChar '0') [] -> halt state
  EvKey KEsc [] -> halt state
  EvKey (KChar '\t') [] -> changeFocus state focusNext
  EvKey KBackTab [] -> changeFocus state focusPrev
  EvKey KEnter [] -> send state
  _ -> handleRest state event
event state _ = continue state

handleRest state event = continue =<< case focusGetCurrent (state ^. mainFocus) of
  Just Message -> f message
  Just Test -> f test
  Nothing -> return state

  where
    f :: Lens' State (Editor Text Name) -> EventM Name State
    f editor = handleEventLensed state editor handleEditorEvent event

send state = do
    let msg = getContent message
    let person = getContent test
    liftIO $ putStrLn "potato"

    result <- liftIO $ sendMessage telegramClient person msg
    liftIO $ print result

    continue $ foldl (flip clearEditor) state [message, test]
  where
    getContent = unlines . getEditContents . (state ^.)

changeFocus state f = continue . (mainFocus %~ f) $ state

clearEditor = (%~ applyEdit clearZipper)
