{-# LANGUAGE TemplateHaskell #-}
module Presentation.State where

import Lens.Micro.TH (makeLenses)
import Brick.Widgets.Edit (Editor)
import Brick.Focus (FocusRing)

data Name = Message | Test
  deriving (Ord, Show, Eq)

data State = State
  { _mainFocus :: FocusRing Name
  , _message :: Editor Text Name
  , _test :: Editor Text Name
  }

makeLenses ''State
