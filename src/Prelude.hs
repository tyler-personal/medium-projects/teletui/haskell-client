module Prelude
  ( module Prelude
  , module BasePrelude
  , module Data.Text
  , module Data.String.Interpolate
  ) where

import BasePrelude hiding (words, unwords, unlines, toUpper, toTitle, toLower, intercalate,
  id)
import Data.Text          (words, unwords, unlines, toUpper, toTitle, toLower, intercalate,
  Text, unpack)

import Data.String.Interpolate

identity f = f

readMay text = readMaybe (unpack text)

(./.) :: (b -> c) -> (a1 -> a2 -> b) -> a1 -> a2 -> c
(./.) = (.) . (.)

