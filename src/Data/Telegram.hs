{-# LANGUAGE OverloadedStrings, NoMonomorphismRestriction, ExtendedDefaultRules, QuasiQuotes, FlexibleContexts, UnicodeSyntax, RankNTypes, NamedFieldPuns #-}
module Data.Telegram (telegramWreqClient) where

import Prelude hiding (lookup)
import Network.Wreq
import Control.Lens
import Data.Aeson
import Data.Aeson.Lens
import Data.Map hiding (map, lookup, toList)
import Data.HashMap.Strict (lookup, HashMap)
import Data.List.Split (splitOn)
import Data.String.Interpolate
import Data.Text (pack)
import Data.Text.Lazy (toStrict)
import Data.Text.Lazy.Encoding (decodeUtf8)
import Data.Vector (Vector)
import Data.ByteString.Lazy (ByteString)

import Domain.Boundaries

getChat' :: FormValue v => v -> IO [(Name, Message)]
getChat' entity = toList . fmap toChat <$> (get' "get_chat" ["name" := entity] <&> (^. responseBody . key "data" . _Array))
  where
    f = toText ./. (fromJust ./. lookup)
    toChat (Object x) = (f "name" x, f "message" x)

telegramWreqClient = TelegramClient
  { sendCode= \phoneNumber -> post' "send_code" ["phone_number" := phoneNumber] <&> statusResponse
  , sendMessage = \name message -> toStrict . decodeUtf8 <$> (post' "send_message" ["name" := name, "message" := message] <&> jsonResponse)
  , getChats = get' "get_chats" [] <&> (^. responseBody . key "data" . _JSON)
  , getChat = getChat'
  }

type Path a = ∀ v. AsValue v => Response v -> a

statusResponse :: Path Text
statusResponse = (^. responseBody . key "status" . _String)

jsonResponse = (^. responseBody)

post' :: Text -> [FormParam] -> IO (Response ByteString)
post' url args = post [i|#{base}/#{url}|] (("token" := token) : args)

get' :: Text -> [FormParam] -> IO (Response ByteString)
get' url args = get [i|#{base}/#{url}?token=#{token}#{params}|]
  where params = toQueryStrings args

base = "http://localhost:8000"
token = "wqkfrdzRYOSEnRFKIicbKThi"

toQueryStrings :: [FormParam] -> Text
toQueryStrings [] = ""
toQueryStrings params = [i|&#{params'}|]
  where params' = intercalate "&" (map toQueryString params)

toQueryString :: FormParam -> Text
toQueryString param = [i|#{key}=#{value}|]
  where (key, value) = splitFormParam param

splitFormParam :: FormParam -> (Text, Text)
splitFormParam param = (items !! 0, items !! 1)
  where items = map pack $ splitOn " := " (show param)

toText (String s) = s
toText _ = error "yeet ya not a string boi"
