module Domain.Boundaries where

type Name = Text
type Message = Text

data TelegramClient = TelegramClient
   { sendCode :: Text -> IO Text
   , sendMessage :: Text -> Text -> IO Text
   , getChats :: IO [Text]
   , getChat :: Text -> IO [(Name, Message)]
   }
